
import React, { Component } from "react";
import {Tab} from "./code-tab"
// import "./styles.css";


const easeInOutCubic = (t, b, c, d) => {
  if ((t /= d / 2) < 1) return (c / 2) * t * t * t + b;
  return (c / 2) * ((t -= 2) * t * t + 2) + b;
};

const animateToTab = index_ => {
  // hljs.initHighlightingOnLoad();

  const scrollView = document.querySelector('.Container .Contents');
  const tabs = document.querySelectorAll('.Container .Contents .Content');
  const nav = document.querySelectorAll('.Container nav a');
  const currentNav = document.querySelector('.Container nav .current');
  let currentTab = 0;
  debugger;
  const index = (index_.index || '#0').replace('#','')
  
  const el = tabs[index];
  const scrollStart = scrollView.scrollLeft;
  const startHeight = scrollView.clientHeight;
  nav[currentTab].classList.remove("active");
  // nav[index].classList.add("active");

  const time = {
    start: performance.now(),
    duration: 700
  };

  const tick = now => {
    time.elapsed = now - time.start;
    const fadeOut = easeInOutCubic(time.elapsed, 1, -1, time.duration);
    const fadeIn = easeInOutCubic(time.elapsed, 0, 1, time.duration);
    debugger;
    const offset = easeInOutCubic(
      time.elapsed,
      scrollStart,
      el.offsetLeft - scrollStart,
      time.duration
    );
    const height = easeInOutCubic(
      time.elapsed,
      startHeight,
      el.clientHeight - startHeight,
      time.duration
    );

    const navOffset = easeInOutCubic(
      time.elapsed,
      nav[currentTab].offsetLeft,
      nav[index].offsetLeft - nav[currentTab].offsetLeft,
      time.duration
    );

    const indicatorWidth = easeInOutCubic(
      time.elapsed,
      nav[currentTab].clientWidth,
      nav[index].clientWidth - nav[currentTab].clientWidth,
      time.duration
    );

    if (currentNav) {
      currentNav.style.transform = `translateX(${navOffset}px)`;
      currentNav.style.width = `${indicatorWidth}px`;
    }

    tabs[currentTab].style.opacity = fadeOut;
    tabs[index].style.opacity = fadeIn;
    scrollView.scrollLeft = offset;
    scrollView.style.height = `${height}px`;

    if (time.elapsed < time.duration) {
      requestAnimationFrame(tick);
    } else {
      currentTab = index;
    }
  };

  requestAnimationFrame(tick);
};

export class CodePanel extends Component {
  handleChangeTab(){
    let index = arguments[0].target.getAttribute('href');
    this.setState({ currentTab: index });
    if (this.currentTab === index) return;
    animateToTab.call(this, { index });
  }


  render() {
  	const languages = this.props.languages;
  	// const languages = ;
  	const arrOfJson = Object.keys(languages);

  	let tabSection = [];

    const navLinks = arrOfJson.map((key, index) => {
	  	const languageConfig = languages[key];
	  	tabSection.push(<Tab code={languageConfig.code} key={languageConfig.index} label={languageConfig.label} id={languageConfig.id} />)
      	return (
	        <a
	          onClick={this.handleChangeTab.bind(this)}
	          href={`#${languageConfig.index}`}
	          data-item={languageConfig.label}
	          className="active"
	          key={languageConfig.index}
	        >
	          {languageConfig.label}
	        </a>
        );
    });
    

  	return (
  		<div>
			<div className="current" />
	      	<nav>{navLinks}</nav>
	      	<div className="Contents">
	        	{tabSection}
	      	</div>
	    </div>
   )
  }
}