
import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CodePanel} from "./code-panel"
import "./styles.css";

// hljs.initHighlightingOnLoad();
const languages = {
  go: {
    id: 0,
    label: 'go',
    index: '0',
    code: `
    stripe.Key = "..."

    chargeParams := &stripe.ChargeParams{
      Amount: 2000,
      Currency: "eur",
      Desc: "Charge for benjamin.wilson@example.com",
    }
    
    // obtained with Stripe.js
    chargeParams.SetSource("tok_mastercard")
    ch, err := charge.New(chargeParams)
    `,
  },
  java: {
    id: 1,
    label: 'python',
    index: '1',
    code: `
    import stripe
stripe.api_key = "..."

stripe.Charge.create(
  amount=2000,
  currency="eur",
  source="tok_amex", # obtained with Stripe.js
  description="Charge for madison.wilson@example.com"
)
    `,
  },
  curl: {
    id: 2,
    label: 'node',
    index: '2',
    code: `
    var stripe = require("stripe")(
      "..."
    );
    
    stripe.charges.create({
      amount: 2000,
      currency: "eur",
      source: "tok_visa", // obtained with Stripe.js
      description: "Charge for jayden.martinez@example.com"
    }, function(err, charge) {
      // asynchronously called
    });
    `,
  }
};


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="Container">
          <CodePanel languages={languages}/>
        </div>
      </div>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
