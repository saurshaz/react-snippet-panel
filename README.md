######################################
### 	react-code-snippets        ###
######################################
- something like `https://apiembed.com/` by mashape
- with help from `postman` and `kongHQ` open-source stuff
- ported from vanilla codepen example to react component version
- right now maybe 85% react dom based


- set it up like 
```

const languages = {
      go: {
        id: 0,
        label: 'go',
        index: '0',
        code: 'go code ...',
      },
      java: {
        id: 1,
        label: 'java',
        index: '1',
        code: 'java code ...',
      },
      curl: {
        id: 2,
        label: 'curl',
        index: '2',
        code: 'curl code ...',
      }
    };
```
and then 

`<CodePanel languages={languages}/>`

![Looks like this](https://i.gyazo.com/27489b22f1fecf187af676f69cbac6d3.gif)